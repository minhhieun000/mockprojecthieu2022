import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  CategoriesService,
  ICategory,
  IProduct,
  ProductsService,
} from '@myorg/products';
import { MessageService } from 'primeng/api';
import { Subject, takeUntil, timer } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'admin-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss'],
})
export class ProductFormComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();
  editMode = false;
  form!: FormGroup;
  isSubmited = false;
  imgDisplay: string | ArrayBuffer | any;
  categories: ICategory[] = [
    { name: 'gg', id: '1' },
    { name: 'gg1', id: '2' },
    { name: 'g2', id: '3' },
  ];
  productId!: string;

  constructor(
    private fb: FormBuilder,
    private categoriesService: CategoriesService,
    private productService: ProductsService,
    private messageService: MessageService,
    private location: Location,
    private router: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this._initForm();
    this._getCategories();
    this._checkEditMode();
  }

  private _initForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      brand: ['', Validators.required],
      price: ['', Validators.required],
      category: ['', Validators.required],
      countInStock: ['', Validators.required],
      description: ['', Validators.required],
      richDescription: [''],
      image: ['', Validators.required],
      isFeatured: [false],
    });
  }

  private _getCategories() {
    this.categoriesService
      .getCategories()
      .pipe(takeUntil(this.endSub$))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }

  get productForm() {
    return this.form.controls;
  }

  onSubmit() {
    this.isSubmited = true;
    if (this.form.invalid) return;
    const productFormData = new FormData();
    Object.keys(this.productForm).map((key) => {
      productFormData.append(key, this.productForm[key].value);
    });

    if (this.editMode) {
      this._updateProduct(productFormData);
    } else {
      this._addProduct(productFormData);
    }
  }

  onCancel() {
    this.location.back();
  }

  private _addProduct(productData: FormData) {
    this.productService
      .createProduct(productData)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        (product: IProduct) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: `Product ${product.name} is created!`,
          });
          timer(2000).subscribe(() => {
            this.location.back();
          });
        },
        (error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Product is not created!',
          });
        }
      );
  }

  onImageUpload(e: any) {
    const file = e.target.files[0];

    if (file) {
      this.form.patchValue({ image: file });
      this.form.get('image')?.updateValueAndValidity();
      const fileReader = new FileReader();
      fileReader.onload = () => {
        this.imgDisplay = fileReader.result;
      };
      fileReader.readAsDataURL(file);
    }
  }

  _checkEditMode() {
    this.router.params.subscribe((param) => {
      if (param.id) {
        this.editMode = true;
        this.productId = param.id;
        this.productService
          .getProduct(param['id'])
          .pipe(takeUntil(this.endSub$))
          .subscribe((product) => {
            this.productForm.name.setValue(product.name);
            this.productForm.category.setValue(product.category?.id);
            this.productForm.brand.setValue(product.brand);
            this.productForm.price.setValue(product.price);
            this.productForm.countInStock.setValue(product.countInStock);
            this.productForm.isFeatured.setValue(product.isFeatured);
            this.productForm.description.setValue(product.description);
            this.productForm.richDescription.setValue(product.richDescription);
            this.imgDisplay = product.image;
            this.productForm.image.setValidators([]);
            this.productForm.image.updateValueAndValidity();
          });
      }
    });
  }

  _updateProduct(productData: FormData) {
    this.productService
      .updateProduct(productData, this.productId)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        (product: IProduct) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: `Product is created!`,
          });
          timer(2000).subscribe(() => {
            this.location.back();
          });
        },
        (error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Product is not created!',
          });
        }
      );
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
