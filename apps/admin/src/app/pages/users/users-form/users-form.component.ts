import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IUser, UsersService } from '@myorg/users';
import { MessageService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';
import { Location } from '@angular/common';
import { timer } from 'rxjs';
import * as countrieslib from 'i18n-iso-countries';

declare const require: any;

@Component({
  selector: 'admin-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss'],
})
export class UsersFormComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();
  form!: FormGroup;
  isSubmitted = false;
  editMode = false;
  userId!: string;
  currentUserId!: string;
  countries: any = [];

  constructor(
    private messageService: MessageService,
    private fb: FormBuilder,
    private location: Location,
    private router: ActivatedRoute,
    private userService: UsersService
  ) {}

  ngOnInit(): void {
    this._checkEditMode();
    this._getCountries();
    this._initForm();
  }

  private _getCountries() {
    countrieslib.registerLocale(require('i18n-iso-countries/langs/en.json'));
    this.countries = Object.entries(
      countrieslib.getNames('en', { select: 'official' })
    ).map((entry) => {
      return {
        id: entry[0],
        name: entry[1],
      };
    });
    console.log(this.countries);
  }

  private _initForm() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required, Validators.email],
      phone: ['', Validators.required],
      isAdmin: [false],
      street: [''],
      apartment: [''],
      zip: [''],
      city: [''],
      country: [''],
    });
  }

  private _checkEditMode() {
    this.router.params.subscribe((param) => {
      if (param.id) {
        this.editMode = true;
        this.userId = param.id;
        this.userService
          .getUser(param['id'])
          .pipe(takeUntil(this.endSub$))
          .subscribe((user) => {
            this.userForm.name.setValue(user.name);
            this.userForm.email.setValue(user.email);
            this.userForm.isAdmin.setValue(user.isAdmin);
            this.userForm.street.setValue(user.street);
            this.userForm.apartment.setValue(user.apartment);
            this.userForm.zip.setValue(user.zip);
            this.userForm.city.setValue(user.city);
            this.userForm.country.setValue(user.country);

            this.userForm.password.setValidators([]);
            this.userForm.password.updateValueAndValidity();
          });
      }
    });
  }

  get userForm() {
    return this.form.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) {
      return;
    }
    const user: IUser = {
      id: this.currentUserId,
      name: this.userForm.name.value,
      email: this.userForm.email.value,
      phone: this.userForm.phone.value,
      isAdmin: this.userForm.isAdmin.value,
      street: this.userForm.street.value,
      apartment: this.userForm.apartment.value,
      zip: this.userForm.zip.value,
      city: this.userForm.city.value,
      country: this.userForm.country.value,
    };
    if (this.editMode) {
      this._updateUser(user);
    } else {
      this._addUser(user);
    }
  }

  private _updateUser(user: IUser) {
    this.userService
      .updateUser(user)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'User is updated!',
          });
          timer(2000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'User is not updated!',
          });
        }
      );
  }

  private _addUser(user: IUser) {
    this.userService
      .createUser(user)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        (user: IUser) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: `User ${user.name} is created!`,
          });
          timer(2000)
            .toPromise()
            .then(() => {
              this.location.back();
            });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'User is not created!',
          });
        }
      );
  }

  onCancel() {
    this.location.back();
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
