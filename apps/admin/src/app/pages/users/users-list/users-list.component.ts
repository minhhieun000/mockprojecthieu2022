import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { IUser, UsersService } from '@myorg/users';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'admin-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();
  users: IUser[] = [
    {
      country: 'VN',
      email: 'h1@gmail.com',
      isAdmin: false,
      name: 'Categories-1',
    },
    {
      country: 'VI',
      email: 'h@gmail.com',
      isAdmin: true,
      name: 'Categories-2',
    },
    {
      country: 'AM',
      email: 'h2@gmail.com',
      name: 'Categories-3',
      isAdmin: false,
    },
  ];

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private usersService: UsersService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getUsers();
  }

  deleteUser(userId: string) {
    this.confirmationService.confirm({
      message: 'Do you warn to delete this user?',
      header: 'Delete User',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.usersService
          .deleteUser(userId)
          .pipe(takeUntil(this.endSub$))
          .subscribe(
            (res) => {
              this._getUsers;
              this.messageService.add({
                severity: 'success',
                summary: 'Success',
                detail: 'Delete User Success',
              });
            },
            (error) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'Delete User False',
              });
            }
          );
      },
    });
  }

  updateUser(userId: string) {
    this.router.navigateByUrl(`users/form/${userId}`);
  }

  private _getUsers() {
    this.usersService
      .getUsers()
      .pipe(takeUntil(this.endSub$))
      .subscribe((users) => {
        this.users = users;
      });
  }

  getCountryName(countryKey: string) {
    if (countryKey) return this.usersService.getCountry(countryKey);
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
