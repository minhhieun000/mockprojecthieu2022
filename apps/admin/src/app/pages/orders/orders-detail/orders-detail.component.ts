import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IOrder, OrdersService } from '@myorg/orders';
import { MessageService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';
import { ORDER_STATUS } from '../order.constants';

@Component({
  selector: 'admin-orders-detail',
  templateUrl: './orders-detail.component.html',
  styleUrls: ['./orders-detail.component.scss'],
})
export class OrdersDetailComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();

  order: IOrder | any = {
    id: '321312asdadqw',
    user: { name: 'hieu Order', email: '@gmail.com' },
    totalPrice: '1200',
    dateOrdered: '6/15/15, 9:03 AM',
    status: '3',
    shippingAddress1: 'ha dong',
    shippingAddress2: 'hoa lac',
    zip: '10000',
    city: 'ha noi 2',
    country: 'vn',
    phone: '03312345678',
    orderItems: [
      {
        product: {
          name: 'sp 5',
          brand: 'sony',
          category: {
            name: 'console',
          },
          price: 199,
        },
        quantity: 2,
      },
      {
        product: {
          name: 'iphone 12 pro',
          brand: 'apple',
          category: {
            name: 'phone',
          },
          price: 399,
        },
        quantity: 4,
      },
    ],
  };

  selectedStatus: any;

  orderStatuses: any = [];
  constructor(
    private ordersService: OrdersService,
    private router: ActivatedRoute,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this._mapOrderStatus();
    this._getOrder();
  }

  private _mapOrderStatus() {
    this.orderStatuses = Object.keys(ORDER_STATUS).map((key) => {
      return {
        id: key,
        name: ORDER_STATUS[key].label,
      };
    });
  }

  private _getOrder() {
    this.router.params.subscribe((params) => {
      if (params.id) {
        this.ordersService
          .getOrder(params.id)
          .pipe(takeUntil(this.endSub$))
          .subscribe((order) => {
            this.order = order;
            this.selectedStatus = order.status;
          });
      }
    });
  }

  onStatusChange(e: any) {
    this.ordersService
      .updateOrder({ status: e.value }, this.order.id)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Order is updated!',
          });
        },
        (error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Order is not updated!',
          });
        }
      );
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
