import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IOrder, OrdersService } from '@myorg/orders';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';
import { ORDER_STATUS } from '../order.constants';

@Component({
  selector: 'admin-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss'],
})
export class OrdersListComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();

  orders: IOrder[] = [
    {
      id: '321312asdadqw',
      user: { name: 'hieu Order', email: '@gmail.com' },
      totalPrice: '100$',
      dateOrdered: '6/15/15, 9:03 AM',
      status: '3',
    },
    {
      id: '321312asdadqw',
      user: { name: 'hieu Order1', email: '@gmail.com' },
      totalPrice: '100$',
      dateOrdered: '6/15/15, 9:03 AM',
      status: '0',
    },
    {
      id: '321312asdadqw',
      user: { name: 'hieu Order2', email: '@gmail.com' },
      totalPrice: '100$',
      dateOrdered: '6/15/15, 9:03 AM',
      status: '4',
    },
  ];

  orderStatus: any = ORDER_STATUS;
  constructor(
    private ordersService: OrdersService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getOrders();
  }

  private _getOrders(): void {
    this.ordersService.getOrders().subscribe((orders) => {
      this.orders = orders;
    });
  }

  deleteOrder(orderId: string) {
    this.confirmationService.confirm({
      message: 'Do you want to Delete this Order?',
      header: 'Delete Order',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.ordersService
          .deleteOrder(orderId)
          .pipe(takeUntil(this.endSub$))
          .subscribe(
            () => {
              this._getOrders();
              this.messageService.add({
                severity: 'success',
                summary: 'Success',
                detail: 'Order is deleted!',
              });
            },
            () => {
              this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'Order is not deleted!',
              });
            }
          );
      },
    });
  }

  showOrder(orderId: string) {
    this.router.navigateByUrl(`orders/${orderId}`);
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
