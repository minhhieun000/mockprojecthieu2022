import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ICategory } from '@myorg/products';
import { CategoriesService } from '@myorg/products';
import { MessageService } from 'primeng/api';
import { Subject, takeUntil, timer } from 'rxjs';

@Component({
  selector: 'admin-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.scss'],
})
export class CategoriesFormComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();
  form!: FormGroup;
  isSubmitted = false;
  editMode = false;
  categoryId: string | any;
  constructor(
    private messageService: MessageService,
    private fb: FormBuilder,
    private categoriesService: CategoriesService,
    private location: Location,
    private router: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      icon: ['', [Validators.required]],
      color: ['#fff'],
    });
    this._checkEditMode();
  }

  onSubmit() {
    this.isSubmitted = true;
    if (this.form.invalid) {
      return;
    } else {
      const category = {
        id: this.categoryId,
        name: this.categoryForm.name.value,
        icon: this.categoryForm.icon.value,
        color: this.categoryForm.color.value,
      };

      if (this.editMode) {
        this._updateCategory(category);
      } else {
        this._addCategory(category);
      }
    }
  }

  onCancel() {
    this.location.back();
  }

  private _updateCategory(category: ICategory) {
    this.categoriesService
      .updateCategory(category)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        () => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Category is updated!',
          });
          timer(2000).subscribe(() => {
            this.location.back();
          });
        },
        () => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Category is not updated!',
          });
          // timer(2000).subscribe(() => {
          //   this.location.back();
          // });
        }
      );
  }

  private _addCategory(category: ICategory) {
    this.categoriesService
      .createCategory(category)
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        (message) => {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'Category is created!',
          });
          timer(2000).subscribe(() => {
            this.location.back();
          });
        },
        (error) => {
          this.messageService.add({
            severity: 'error',
            summary: 'Error',
            detail: 'Category is not created!',
          });
        }
      );
  }

  private _checkEditMode() {
    this.router.params.subscribe((param) => {
      if (param.id) {
        this.editMode = true;
        this.categoryId = param.id;
        this.categoriesService
          .getCategory(param['id'])
          .pipe(takeUntil(this.endSub$))
          .subscribe((category) => {
            this.categoryForm.name.setValue(category.name);
            this.categoryForm.icon.setValue(category.icon);
            this.categoryForm.color.setValue(category.color);
          });
      }
    });
  }

  get categoryForm() {
    return this.form.controls;
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
