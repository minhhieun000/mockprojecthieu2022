import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoriesService, ICategory } from '@myorg/products';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'admin-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.scss'],
})
export class CategoriesListComponent implements OnInit, OnDestroy {
  categories: ICategory[] = [
    {
      id: '1',
      name: 'Categories-1',
      icon: 'apple',
      color: '#FFFFFF',
    },
    {
      id: '2',
      name: 'Categories-2',
      icon: 'bitcoin',
      color: '#ffe0cc',
    },
    {
      id: '3',
      name: 'Categories-3',
      icon: 'cloud',
      color: '#hhh',
    },
  ];

  endSub$: Subject<any> = new Subject();

  constructor(
    private messageService: MessageService,
    private CategoriesService: CategoriesService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this._getCategories();
  }

  deleteCategory(categoryId: string) {
    this.confirmationService.confirm({
      message: 'Do you warn to delete this category?',
      header: 'Delete Category',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.CategoriesService.deleteCategory(categoryId)
          .pipe(takeUntil(this.endSub$))
          .subscribe(
            (res) => {
              this._getCategories;
              this.messageService.add({
                severity: 'success',
                summary: 'Success',
                detail: 'Delete Category Success',
              });
            },
            (error) => {
              this.messageService.add({
                severity: 'error',
                summary: 'Error',
                detail: 'Delete Category False',
              });
            }
          );
      },
    });
  }

  updateCategory(categoryId: string) {
    this.router.navigateByUrl(`/categories/form/${categoryId}`);
  }

  private _getCategories() {
    this.CategoriesService.getCategories()
      .pipe(takeUntil(this.endSub$))
      .subscribe((catsData) => {
        this.categories = catsData;
      });
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
