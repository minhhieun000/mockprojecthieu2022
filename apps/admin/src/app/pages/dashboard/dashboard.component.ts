/* eslint-disable @typescript-eslint/no-empty-function */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { OrdersService } from '@myorg/orders';
import { ProductsService } from '@myorg/products';
import { UsersService } from '@myorg/users';
import { combineLatest, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'admin-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  endSub$: Subject<any> = new Subject();

  statistics = [10, 11, 12, 123];
  constructor(
    private userService: UsersService,
    private productService: ProductsService,
    private ordersService: OrdersService
  ) {}

  ngOnInit(): void {
    combineLatest([
      this.ordersService.getOrdersCount(),
      this.productService.getProductsCount(),
      this.userService.getUsersCount(),
      this.ordersService.getTotalSales(),
    ])
      .pipe(takeUntil(this.endSub$))
      .subscribe((values) => {
        this.statistics = values;
      });
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
