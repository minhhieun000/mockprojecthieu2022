import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Categories } from '../../core/indto/categories.indto';
import { Product } from '../../core/indto/products.indto';
import { CategoriesService } from '../../core/services/categories.service';
import { ProductServices } from '../../core/services/products.service';

@Component({
  selector: 'myorg-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit, OnDestroy {
  productItems: Product[] = [];
  unSubcirbe$ = new Subject();
  categories: Categories[] = [];
  isCategories!: boolean;
  // checked: boolean = false;
  constructor(
    private productService: ProductServices,
    private categorieService: CategoriesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.route.params.subscribe((param) => {
      param.id ? this.getProducts() : this.getProducts();
      param.id ? (this.isCategories = true) : (this.isCategories = false);
    });
    this.getCategories();
    // this.getProducts();
    console.log(this.getProducts(['63589e4935ea237ff0835c50']));
  }

  getProducts(category?: string[]) {
    this.productService
      .getProduct(category)
      .pipe(takeUntil(this.unSubcirbe$))
      .subscribe((products) => {
        this.productItems = products;
        // console.log(products);
      });
  }

  getCategories() {
    this.categorieService
      .getCategories()
      .pipe(takeUntil(this.unSubcirbe$))
      .subscribe((categgories) => {
        this.categories = categgories;
      });
  }

  categoriesFilter() {
    const selectedCategories = this.categories
      .filter((cate) => cate.checked)
      .map((category) => category.id);
    this.getProducts(selectedCategories);
    console.log(selectedCategories);
    // console.log(this.getProducts(selectedCategories));
  }

  ngOnDestroy(): void {
    this.unSubcirbe$.unsubscribe();
  }
}
