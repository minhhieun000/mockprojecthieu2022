import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Categories } from '../../../core/indto/categories.indto';
import { CategoriesService } from '../../../core/services/categories.service';

@Component({
  selector: 'hshop-categories',
  templateUrl: './ui-categories.component.html',
  styleUrls: ['./ui-categories.component.scss'],
})
export class UiCategoriesComponent implements OnInit, OnDestroy {
  categories: Categories[] = [];
  endSub$: Subject<any> = new Subject();

  constructor(private categoriesService: CategoriesService) {}

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.categoriesService
      .getCategories()
      .pipe(takeUntil(this.endSub$))
      .subscribe(
        (categories) => {
          this.categories = categories;
        },
        (err) => {
          console.log(err);
        }
      );
  }

  ngOnDestroy(): void {
    this.endSub$.unsubscribe();
  }
}
