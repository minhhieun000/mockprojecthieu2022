import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Product } from '../../../core/indto/products.indto';
import { ProductServices } from '../../../core/services/products.service';

@Component({
  selector: 'hshop-products',
  templateUrl: './ui-products.component.html',
  styleUrls: ['./ui-products.component.scss'],
})
export class UiProductsComponent implements OnInit, OnDestroy {
  endSubcribe$ = new Subject();
  featuredProduct: Product[] = [];
  // endSubcribe$: any;
  count: number = 1;
  constructor(private productService: ProductServices) {}

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData() {
    this.productService
      .getFeaturedProduct(4)
      .pipe(takeUntil(this.endSubcribe$))
      .subscribe(
        (products) => {
          this.featuredProduct = products;
        },
        (err) => {
          console.log(err);
        }
      );
  }

  ngOnDestroy(): void {
    this.endSubcribe$.unsubscribe();
  }
}
