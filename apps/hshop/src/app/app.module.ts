import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AccordionModule } from 'primeng/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputTextModule } from 'primeng/inputtext';
import { NavComponent } from './shared/nav/nav.component';
import { UiBannerComponent } from './pages/home/ui-banner/ui-banner.component';
import { UiCategoriesComponent } from './pages/home/ui-categories/ui-categories.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UiProductsComponent } from './pages/home/ui-products/ui-products.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProductsModule } from '@myorg/products';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { ProductItemPageComponent } from './pages/product-list/product-item-page/product-item-page.component';

// import { UiModule } from '@myorg/ui';

export function httpLoaderfactor(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/core/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductListComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    UiBannerComponent,
    UiCategoriesComponent,
    UiProductsComponent,
    ProductItemPageComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    AccordionModule,
    InputTextModule,
    HttpClientModule,
    ProgressSpinnerModule,
    ProductsModule,
    CheckboxModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderfactor,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
