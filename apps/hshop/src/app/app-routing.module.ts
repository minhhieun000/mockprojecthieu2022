import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ProductItemPageComponent } from './pages/product-list/product-item-page/product-item-page.component';
import { ProductListComponent } from './pages/product-list/product-list.component';

const routers: Routes = [
  { path: '', component: HomeComponent },
  { path: 'product', component: ProductListComponent },
  { path: 'product/productId', component: ProductItemPageComponent },
  { path: 'category/:id', component: ProductListComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routers, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
