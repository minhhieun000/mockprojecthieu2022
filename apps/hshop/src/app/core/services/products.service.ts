import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'apps/hshop/src/environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../indto/products.indto';

@Injectable({
  providedIn: 'root',
})
export class ProductServices {
  apiProduct = environment.apiUrl + 'products';
  constructor(private http: HttpClient) {}

  getProduct(categoryFilter?: string[]): Observable<Product[]> {
    let params = new HttpParams();
    if (categoryFilter) {
      params = params.append('categories', categoryFilter.join(','));
    }
    return this.http.get<Product[]>(`${this.apiProduct}`, { params });
  }

  getFeaturedProduct(count: number): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.apiProduct}/get/featured/${count}`);
  }
}
