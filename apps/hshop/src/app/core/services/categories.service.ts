import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'apps/hshop/src/environments/environment';
import { Categories } from '../indto/categories.indto';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  api = environment.apiUrl + 'categories';
  constructor(private http: HttpClient) {}

  getCategories(): Observable<Categories[]> {
    return this.http.get<Categories[]>(this.api);
  }
}
