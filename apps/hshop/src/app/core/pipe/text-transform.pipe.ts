import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textTransform',
})
export class TextTransformPipe implements PipeTransform {
  transform(value: string, length: number): any {
    if (value && value.length > length) {
      return value.slice(0, length) + '...';
    } else if (value && value.length <= length) {
      return value;
    }
  }
}
