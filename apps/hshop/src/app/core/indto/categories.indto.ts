export interface Categories {
  id: string;
  name: string;
  icon: string;
  color: string;
  checked?: boolean;
}
