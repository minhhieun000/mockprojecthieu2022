export interface Product {
  id: string;
  name: string;
  description: string;
  richDescription: string;
  image?: string;
  brand?: string;
  price: number;
  category: string;
  countInStock?: number;
  rating?: number;
  numRevires?: number;
  isFeatrued?: boolean;
}
