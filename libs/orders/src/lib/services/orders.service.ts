import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '@env/environment';
import { IOrder } from '@myorg/orders';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  apiUrlOrders = environment.apiUrl + 'orders';
  constructor(private http: HttpClient) {}
  getOrders(): Observable<IOrder[]> {
    return this.http.get<IOrder[]>(`${this.apiUrlOrders}`);
  }

  getOrder(orderId: string): Observable<IOrder> {
    return this.http.get<IOrder>(`${this.apiUrlOrders}/${orderId}`);
  }

  createOrder(order: IOrder): Observable<IOrder> {
    return this.http.post<IOrder>(`${this.apiUrlOrders}`, order);
  }

  updateOrder(
    orderStatus: { status: string },
    orderId: IOrder
  ): Observable<IOrder> {
    return this.http.put<IOrder>(
      `${this.apiUrlOrders}/${orderId}`,
      orderStatus
    );
  }

  deleteOrder(orderId: string): Observable<object> {
    return this.http.delete<object>(`${this.apiUrlOrders}/${orderId}`);
  }

  getOrdersCount(): Observable<number> {
    return this.http
      .get<number>(`${this.apiUrlOrders}/get/count`)
      .pipe(map((objectValue: any) => objectValue.orderCount));
  }

  getTotalSales(): Observable<number> {
    return this.http
      .get<number>(`${this.apiUrlOrders}/get/totalsales`)
      .pipe(map((objectValue: any) => objectValue.totalsales));
  }
}
