import { IUser } from '@myorg/users';

export interface IOrder {
  id?: string;
  orderItems?: IOrderItem[];
  shippingAddress1?: string;
  shippingAddress2?: string;
  city?: string;
  zip?: string;
  country?: string;
  phone?: string;
  status?: string;
  totalPrice?: string;
  user?: IUser;
  dateOrdered?: string;
}

export interface IOrderItem {
  product?: {
    name?: string;
    brand?: string;
    category?: {
      name?: string;
    };
    price?: number;
  };
  quantity?: number;
}
