import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'apps/hshop/src/app/core/indto/products.indto';

@Component({
  selector: 'product-items',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss'],
})
export class ProductItemComponent implements OnInit {
  @Input() product!: Product;
  constructor() {}

  ngOnInit(): void {}
}
