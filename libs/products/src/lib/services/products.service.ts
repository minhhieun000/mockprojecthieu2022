import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '@env/environment';
import { IProduct } from '../models/product';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  apiUrlproducts = environment.apiUrl + 'products';

  constructor(private http: HttpClient) {}

  getproducts(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(`${this.apiUrlproducts}`);
  }

  getProduct(productId: string): Observable<IProduct> {
    return this.http.get<IProduct>(`${this.apiUrlproducts}/${productId}`);
  }

  createProduct(product: FormData): Observable<IProduct> {
    return this.http.post<IProduct>(`${this.apiUrlproducts}`, product);
  }

  updateProduct(
    productData: FormData,
    productId: string
  ): Observable<IProduct> {
    return this.http.put<IProduct>(
      `${this.apiUrlproducts}/${productId}`,
      productData
    );
  }

  deleteProduct(productId: string): Observable<any> {
    return this.http.delete<any>(`${this.apiUrlproducts}/${productId}`);
  }

  getProductsCount(): Observable<number> {
    return this.http
      .get<number>(`${this.apiUrlproducts}/get/count`)
      .pipe(map((objectValue: any) => objectValue.productCount));
  }
}
