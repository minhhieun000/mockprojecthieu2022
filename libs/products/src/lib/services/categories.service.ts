import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ICategory } from '../models/category';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root',
})
export class CategoriesService {
  apiUrlCategories = environment.apiUrl + 'categories';
  constructor(private http: HttpClient) {}
  getCategories(): Observable<ICategory[]> {
    return this.http.get<ICategory[]>(`${this.apiUrlCategories}`);
  }

  getCategory(categoryId: string): Observable<ICategory> {
    return this.http.get<ICategory>(`${this.apiUrlCategories}/${categoryId}`);
  }

  createCategory(category: ICategory): Observable<ICategory> {
    return this.http.post<ICategory>(`${this.apiUrlCategories}`, category);
  }

  updateCategory(category: ICategory): Observable<ICategory> {
    return this.http.put<ICategory>(
      `${this.apiUrlCategories}/${category.id}`,
      category
    );
  }

  deleteCategory(categoryId: string): Observable<object> {
    return this.http.delete<object>(`${this.apiUrlCategories}/${categoryId}`);
  }
}
