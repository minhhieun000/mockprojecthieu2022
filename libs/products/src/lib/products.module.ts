import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductItemComponent } from './components/product/product-item/product-item.component';
import { ProductItemPageComponent } from 'apps/hshop/src/app/pages/product-list/product-item-page/product-item-page.component';
import { TextTransformPipe } from 'apps/hshop/src/app/core/pipe/text-transform.pipe';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'product/:productId', component: ProductItemPageComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  declarations: [ProductItemComponent, TextTransformPipe],
  exports: [ProductItemComponent],
})
export class ProductsModule {}
