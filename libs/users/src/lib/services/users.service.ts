import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '@env/environment';
import { IUser } from '../models/user';
import * as countrieslib from 'i18n-iso-countries';
import { map } from 'rxjs';
declare const require: any;

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  apiUrlUsers = environment.apiUrl + 'users';
  constructor(private http: HttpClient) {
    countrieslib.registerLocale(require('i18n-iso-countries/langs/en.json'));
  }
  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${this.apiUrlUsers}`);
  }

  getUser(userId: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.apiUrlUsers}/${userId}`);
  }

  createUser(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(`${this.apiUrlUsers}`, user);
  }

  updateUser(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(`${this.apiUrlUsers}/${user}`, user);
  }

  deleteUser(userId: string): Observable<object> {
    return this.http.delete<object>(`${this.apiUrlUsers}/${userId}`);
  }

  getCountries(): { id: string; name: string }[] {
    return Object.entries(
      countrieslib.getNames('en', { select: 'official' })
    ).map((entry) => {
      return {
        id: entry[0],
        name: entry[1],
      };
    });
  }

  getCountry(countryKey: string) {
    return countrieslib.getName(countryKey, 'en');
  }

  getUsersCount(): Observable<number> {
    return this.http
      .get<number>(`${this.apiUrlUsers}/get/count`)
      .pipe(map((objectValue: any) => objectValue.userCount));
  }
}
