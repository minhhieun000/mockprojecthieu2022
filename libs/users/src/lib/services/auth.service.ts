import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { IUser } from '@myorg/users';
import { Observable } from 'rxjs';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  apiURLUsers = environment.apiUrl + 'users';

  constructor(
    private http: HttpClient,
    private router: Router,
    private localstorageService: LocalstorageService
  ) {}

  login(email: string, password: string): Observable<IUser> {
    return this.http.post<IUser>(`${this.apiURLUsers}/login`, {
      email,
      password,
    });
  }

  logout() {
    // this.token.removeToken();
    this.router.navigateByUrl('/login');
  }
}
