import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LocalstorageService } from '../../services/localstorage.service';

@Component({
  selector: 'user-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  isSubmitted = false;
  authError = false;
  authMessage = 'Email or Password are wrong';

  constructor(
    private fb: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private localstorageService: LocalstorageService
  ) {}

  ngOnInit(): void {
    this._initLoginForm();
  }
  private _initLoginForm(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.required, Validators.email],
      password: ['', Validators.required],
    });
  }

  get loginF() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.loginForm.invalid) {
      this.auth
        .login(this.loginF.email.value, this.loginF.password.value)
        .subscribe(
          (user) => {
            this.authError = false;
            this.localstorageService.setToken(user.token);
            this.router.navigateByUrl('/');
          },
          (error: HttpErrorResponse) => {
            this.authError = true;
            if (error.status !== 400) {
              this.authMessage = 'Error in the Server, please try again later!';
            }
          }
        );
    } else {
      return;
    }
  }
}
